import json
from datatableview.utils import DatatableOptions, DatatableStructure
from datatableview.views import DatatableView
from django.http import HttpResponse
from django.template.loader import render_to_string


class AjaxEnabledResponseMixin(object):
    success_message = "Your information has been saved."

    @staticmethod
    def render_to_json_response(context, **response_kwargs):
        data = json.dumps(context)
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(data, **response_kwargs)

    def form_invalid(self, form):
        response = super(AjaxEnabledResponseMixin, self).form_invalid(form)
        if self.request.is_ajax():
            return self.render_to_json_response(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        response = super(AjaxEnabledResponseMixin, self).form_valid(form)
        if self.request.is_ajax():
            data = {
                "message": self.success_message,
                "object": {
                    'pk': self.object.pk
                }

            }
            return self.render_to_json_response(data)
        else:
            return response


class ActionsEnabledDatatableStructure(DatatableStructure):
    def __init__(self, ajax_url, options, model, actions):
        super(self.__class__, self).__init__(ajax_url, options, model)
        self.actions = actions

    def __unicode__(self):
        return render_to_string(self.options['structure_template'], {
            'url': self.url,
            'actions': json.dumps(self.actions),
            'status_column': 0,
            'result_counter_id': self.options['result_counter_id'],
            'column_info': self.get_column_info(),
        })


class ActionsEnabledDatatableView(DatatableView):
    actions = ""
    status_column = 0

    def get_datatable(self):
        options = self._get_datatable_options()

        if not isinstance(options, DatatableOptions):
            options = DatatableOptions(self.model, {}, **options)
        return ActionsEnabledDatatableStructure(self.request.path, options, model=self.model, actions=self.actions)


