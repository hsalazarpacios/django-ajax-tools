function _debug(msg) {
    if (window.console && window.console.debug) {
        window.console.debug(msg);
    }
    else if (window.opera && window.opera.postError) {
        window.opera.postError(msg);
    }
}
init.push(function () {
    var _form = $('form[data-ajax-enabled="true"]'),
        _submit = _form.find('button[type="submit",input[type="submit"],.btn[type="submit"]');

    _form.validate({ focusInvalid: false});
    _form.ajaxForm({
        beforeSubmit: function (arr, $form, options) {
            _submit.button("loading");
        },
        success: function (response) {
            $.growl.notice({ message: response.message });
        },
        error: function (jqXHR) {
            switch (jqXHR.status) {
                case 400:
                {
                    $.growl.error({ message: "There it was a problem with the data provided" });
                    $.each(jqXHR.responseJSON, function (index, message) {
                        var field = _form.find('input[name="' + index + '"],textarea[name="' + index + '"],select[name="' + index + '"]'),
                            error = $('<div for="id_' + index + '" class="help-block jquery-validate-error" >' + message[0] + '</div >');
                        error.insertAfter(field);
                        field.parents(".form-group").addClass("has-error");
                    });
                    break;
                }
                case 500:
                {
                    $.growl.error({ message: "Something went wrong on the server side. Contact an administrator." });
                    break;
                }

            }
        },
        complete: function () {
            _submit.button("reset")
        }

    });
});


datatableview.auto_initialize = false;
init.push(function () {
    $("select.select2").select2();
    var common_options = {
        "sDom": "<'table-header clearfix'<'pull-right table-actions btn-toolbar'><'DT-lf-left'<'DT-per-page'l><'DT-search'f>>r>" +
            "t" +
            "<'table-footer clearfix'<'DT-label'i><'DT-pagination'p>>",
        oLanguage: {
            "sSearch": "",
            "sLengthMenu": "_MENU_"
        }
    };
    var tables = $('.datatable');
    var test = datatableview.initialize(tables, common_options);

    $('.dataTables_filter input').attr('placeholder', 'Search...');
    $('.dataTables_filter .clear-search').remove();
    tables.attr("width", "100%");
    tables.on("click", "tr", function (e) {
        e.stopPropagation();
        var row = $(this);
        if (!row.hasClass("selected-row")) {
            $("tr.selected-row").removeClass("selected-row").trigger("row-unselected");
            row.addClass("selected-row").trigger("row-selected");
        }

    });
    $(document).click(function () {
        tables.find("tr.selected-row").removeClass("selected-row").trigger("row-unselected");
    });

    $.each(tables, function (index, table) {
        var actions = JSON.parse($(table).attr("data-actions"));
        var container = $(table).siblings("div.table-header").find("div.table-actions");
        $.each(actions, function (index, action) {
            var btn = $('<a class="btn btn-default disabled" href="#"></a>')
                .attr({
                    "data-url-pattern": (action.hasOwnProperty("url-pattern")) ? action["url-pattern"] : "",
                    "href": (action.hasOwnProperty("url")) ? action["url"] : "#",
                    "title": (action.hasOwnProperty("title")) ? action["title"] : "",
                    "data-status-allowed": (action.hasOwnProperty("status_allowed")) ? action["status_allowed"] : "all"
                });

            if (action.hasOwnProperty("enabled") && action.enabled == true)
                btn.removeClass("disabled");

            if (action.hasOwnProperty("icon") && action.icon !== null && action.icon !== "") {
                btn.addClass("btn-labeled");
                btn.append('<span class="btn-label icon ' + action.icon + '"></span> ');
            }

            if (action.hasOwnProperty("text") && action.text !== null && action.text !== "")
                btn.append(action.text);
            btn.appendTo(container);
            btn.tooltip();
            $(table).on("row-selected", "tr", function (e) {
                if (btn.attr("data-status-allowed") !== "UnSelected") {
                    btn.attr("href", btn.attr("data-url-pattern").replace("PK_PLACEHOLDER", $(this).attr("id")));
                    btn.removeClass("disabled")

                }
            });
            $(table).on("row-unselected", "tr", function (e) {
                if (btn.attr("data-status-allowed") !== "UnSelected") {
                    btn.addClass("disabled")
                    btn.attr("href", "#")
                }
            });
            btn.on("click", function (e) {
                e.stopPropagation()
            })
        });

    });
});